

def game_score(rolls):
    def is_strike():
        return rolls[roll_num] == 10

    def is_spare():
        return rolls[roll_num] + rolls[roll_num + 1] == 10

    score = 0
    roll_num = 0
    for frame in range(10):
        if is_strike():
            score += 10 + rolls[roll_num + 1] + rolls[roll_num + 2]
            roll_num += 1
        elif is_spare():
            score += 10 + rolls[roll_num + 2]
            roll_num += 2
        else:
            score += rolls[roll_num] + rolls[roll_num + 1]
            roll_num += 2
    return score


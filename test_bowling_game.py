#!/usr/bin/env python
import unittest

from bowling_game import game_score


class TestGame(unittest.TestCase):
    def test_gutter_game(self):
        rolls = [0] * 20
        score = game_score(rolls)
        self.assertEqual(0, score)

    def test_all_ones(self):
        rolls = [1] * 20
        score = game_score(rolls)
        self.assertEqual(20, score)

    def test_one_spare(self):
        rolls = [5, 5, 3] + [0] * 17
        score = game_score(rolls)
        self.assertEqual(16, score)

    def test_one_strike(self):
        rolls = [10, 5, 3] + [0] * 16
        score = game_score(rolls)
        self.assertEqual(26, score)

    def test_perfect_game(self):
        rolls = [10] * 12
        score = game_score(rolls)
        self.assertEqual(300, score)
